<?php while (have_posts()) : the_post(); ?>
	<div class="row">
		<div class="col-sm-8">
			<h1 class="section-head"><?php the_title(); ?></h1>
			<?php the_content(); ?>
		</div>
		<div class="col-sm-4">
			<?php the_post_thumbnail('full'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-10">
		<?php
			
			$posts_query = new WP_Query(array(
				'post_type' => 'post',
				'posts_per_page' => -1,
			));
			while ($posts_query->have_posts()): $posts_query->the_post();
				get_template_part('templates/content');
			endwhile;
			wp_reset_postdata();
			
			
			?>
		</div>
	</div>
<?php endwhile; ?>
