<footer class="content-info" role="contentinfo">
	<div class="container">
		<nav role="navigation" class="clearfix">
			<?php
				if (has_nav_menu('footer_navigation')) :
					wp_nav_menu(array('theme_location' => 'footer_navigation', 'menu_class' => 'nav navbar-nav'));
				endif;
			?>
		</nav>

		<div class="row">
			<div class="col-sm-6">
				<p><?php echo str_replace('|', '<span class="sep">|</span>', get_field('footer_text', 'options')); ?></p>
			</div>
			<div class="col-sm-6">
				<p class="copyright"><?php the_field('copyright', 'options'); ?></p>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
