<?php while (have_posts()) : the_post(); ?>
<article class="hentry cans-cooking-entry">
	<div class="row">
		<div class="col-sm-6 hidden-xs">
			<?php the_post_thumbnail('full', array( 'class' => 'alignright' )); ?>
		</div>

		<div class="col-sm-6">
			<header>
				<h1 class="section-title"><?php the_title(); ?></h1>
			</header>
			<div class="entry-content"><?php the_content(); ?></div>
			
			
			<?php if (get_field('navigation_slider')): ?>
			<footer>
				<div class="carouselslider">
					<ul class="slides">
						<?php while (have_rows('navigation_slider')): the_row(); ?>
						<li><a href="<?php the_sub_field('page'); if (get_sub_field('hash_section')) echo '#' . get_sub_field('hash_section'); ?>"><?php
							echo wp_get_attachment_image(get_sub_field('image'), 'full');
						?></a></li>
						<?php endwhile; ?>
					</ul>
				</div>
			</footer>
			<?php endif; ?>
		</div>
	</div>
</article>

<section class="one-section cooking-section-1">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-9 toptab-section">
				<h4 class="toptab-long"><?php the_field('tab_1_title'); ?></h4>
				<div class="toptab-content">
					<h5 class="section-subhead4"><?php the_field('tab_1_text') ?></h5>
				</div>
			</div>
		</div>

		<div class="arrow-section-h-wrap">
			<?php
			$section5List_index = 1;
			while (have_rows('list_items')): the_row(); ?>
			<section class="arrow-section-h">
				<div class="a-title">
					<h1><?php the_sub_field('title') ?></h1>
				</div>
				<div class="a-arrow">
					<span class="arrow-count"><?php echo $section5List_index++; ?></span>
				</div>
				<div class="a-content"><p><?php the_sub_field('text'); ?></p>
					<a class="a-link" href="<?php the_sub_field('link'); ?>"><?php the_sub_field('link_text'); ?></a>
				</div>
			</section>
			<?php endwhile; ?>
		</div>

		<h4 class="toptab">Videos</h4>
		<div class="clear"></div>

		<div class="row">
		<?php while (have_rows('video_items')) : the_row(); ?>
			<div class="col-sm-4 video-item">
				<a href="<?php the_sub_field('video_link'); ?>">
					<span class="video-title"><?php the_sub_field('title'); ?></span>
					<span class="video-thumb"><img src="<?php the_sub_field('thumbnail'); ?>" alt=""></span>
					<span class="video-source">watch on <?php the_sub_field('source'); ?></span>
				</a>
			</div>
		<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endwhile; ?>
