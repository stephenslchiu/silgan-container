<?php while (have_posts()) : the_post(); ?>





	<section class="image-sections">
		<?php if (get_field('image_sections')) while (have_rows('image_sections')) {
			the_row();
		?>
		
		<div class="row">
			<div class="col-sm-<?php the_sub_field('left_column_size'); ?>">
				<div>
					<?php if (get_sub_field('special_video_module')) { ?>
					<div class="special-video">
						<div>
							<?php echo do_shortcode('[video_lightbox_youtube video_id="' . get_sub_field('left_link') . '" width="640" height="480" anchor="' . wp_get_attachment_url(get_sub_field('left_image')) . '"]'); ?>
							

							
							<h5>Feature Video</h5>
							
							<p>Share: <span class="myshare"></span> </p>
						</div>
					</div>
					<?php } else {
						echo wp_get_attachment_image(get_sub_field('left_image'), 'full');

						if (get_sub_field('left_link_text')):
					?>
					<a class="link-overlay" href="<?php the_sub_field('left_link') ?>"><span><?php the_sub_field('left_link_text') ?></span></a>
					<?php
						endif;
					}
					?>
				</div>
			</div>
			<div class="col-sm-<?php the_sub_field('right_column_size'); ?>">
				<div>
					<?php echo wp_get_attachment_image(get_sub_field('right_image'), 'full');
					if (get_sub_field('right_link_text')): ?>
					<a class="link-overlay" href="<?php the_sub_field('right_link') ?>"><span><?php the_sub_field('right_link_text') ?></span></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
		
		<?php } ?>
	</section>
<?php endwhile; ?>
