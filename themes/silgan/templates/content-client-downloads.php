<?php while (have_posts()) : the_post(); ?>

<section class="one-section client-downloads-section-1">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6">
				<h2 class="section-head"><?php the_field('section_1_title'); ?></h2>
				<?php echo wpautop(get_field('section_1_text_1')); ?>
				<h3 class="section-subhead"><?php the_field('section_1_subtitle'); ?></h3>
				<?php echo wpautop(get_field('section_1_text_2')); ?>
			</div>
			<div class="col-sm-6">
				<img src="<?php the_field('section_1_image'); ?>" alt="" class="section-image">
			</div>
		</div>
	</div>
</section>


<?php endwhile; ?>
