<?php while (have_posts()) : the_post(); ?>

<section class="one-section vendors-section-1">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-5">
				<h3 class="section-head"><?php the_field('section_1_title'); ?></h3>
				<?php echo wpautop(get_field('section_1_text')); ?>
			</div>
			<div class="col-sm-7">
				<img src="<?php the_field('section_1_image'); ?>" alt="" class="section-image">
			</div>
		</div>
	</div>
</section>


<section class="one-section vendors-section-2">
	<div class="section-wrap">
		<h4 class="toptab-logo">Logos</h4>

		<div class="row download-list">
			<div class="col-sm-6">
			<?php
			$download_index = 0;
			while (have_rows('section_2_downloads')) { the_row();
				if ($download_index != 0) echo '</div><div class="col-sm-6">';
			?>
			<dl>
				<dt><h3><?php the_sub_field('title'); ?></h3></dt>
				<?php while (have_rows('files')) { the_row();
					$myfile = get_sub_field('file');
					if ($myfile) {
						$file_url = pathinfo($myfile['url']);
						if ($file_url['basename']) {
							echo '<dd><a href="' . $myfile['url'] . '">' . $file_url['basename'] . '</a><b class="download"></b></dd>';
						}
					}
				} ?>
				<?php
				$zip_file = get_sub_field('zip_file');
				if ($zip_file) {
					$zip_url = pathinfo($zip_file['url']);
					if ($zip_url['basename']) {
						echo '<dd>Download all the above in a zip file [ <a href="' . $zip_file['url'] . '">' . $zip_url['basename'] . '</a> ]<b class="download"></b></dd>';
					}
				}
				?>
			</dl>
			<?php
				$download_index++;
			} ?>
			</div>
		</div>
	</div>
</section>



<?php endwhile; ?>
