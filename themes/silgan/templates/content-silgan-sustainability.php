<?php while (have_posts()) : the_post(); ?>


<?php get_template_part('templates/page', 'section-nav'); ?>



<section class="one-section sus-section-1 hidden-xs">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-7">
				<h1 class="section-title"><?php the_field('section_1_title'); ?></h1>
				<h2 class="section-subhead3"><?php the_field('section_1_subtitle'); ?></h2>

				<?php echo wpautop(get_field('section_1_text_1')); ?>
				<div class="section-ribbon"><?php the_field('section_1_ribbon_text'); ?></div>
				<?php echo wpautop(get_field('section_1_text_2')); ?>
			</div>
		</div>

		<img src="<?php the_field('section_1_image'); ?>" alt="" class="section-image">
	</div>
</section>



<section class="one-section sus-section-2">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="section-head"><?php the_field('section_2_title'); ?></h2>
				<?php echo wpautop(get_field('section_2_text')); ?>
			</div>
		</div>

		<div class="section-image-wrap">
			<img src="<?php the_field('section_2_image'); ?>" alt="" class="section-image">

			<?php $section_image_2_index = 0; ?>
			<?php while (have_rows('section_2_image_content')): the_row(); ?>
			<div class="circle-text circle-<?php echo ++$section_image_2_index; ?>"><?php the_sub_field('text'); ?></div>
			<?php endwhile; ?>
		</div>

		<div class="contentsecond"><?php the_field('section_2_text_2'); ?></div>

		<h3 class="recycle-head"><?php the_field('section_2_section_list_title'); ?></h3>
		<div class="recycle-section">
		<?php while (have_rows('section_2_section_list')): the_row(); ?>
			<div class="recycle-item">
				<div class="item-1"><?php the_sub_field('text_1'); ?></div>
				<div class="item-2"><?php the_sub_field('text_2'); ?></div>
			</div>
		<?php endwhile; ?>
		</div>
		<footer><?php the_field('section_2_footer'); ?></footer>
	</div>
</section>



<section class="one-section sus-section-3 hidden-xs">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="section-head"><?php the_field('section_3_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_3_subtitle'); ?></h3>
				<div class="contentfirst"><?php echo wpautop(get_field('section_3_text')); ?></div>
			</div>

			<div class="col-sm-3">
				<img src="<?php the_field('section_3_image'); ?>" alt="" class="section-image">
			</div>
			<div class="col-sm-9">
				<?php if (get_field('section_3_table')): $table_index = 0; ?>
				<table class="section-table">
					<?php while (have_rows('section_3_table')) {
						the_row();
						if ($table_index % 2 == 0) echo '<tr>';
						echo '<td><div>' . get_sub_field('text') . '</div></td>';
						if ($table_index % 2 == 1) echo '</tr>';
						$table_index++;
					} ?>
				</table>
				<?php endif; ?>
			</div>
		</div>

		<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/hshadow.png" alt="" class="aligncenter">

		<h3 class="section-line-head"><span><?php the_field('section_3_chain_title'); ?></span></h3>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="contentsecond"><p><?php the_field('section_3_chain_text'); ?></p></div>
			</div>
		</div>
		<?php if (have_rows('section_3_chain_items')): $chain_index = 0; ?>
		<div class="row chain-items">
			<?php while (have_rows('section_3_chain_items')) {
				the_row();
			?>
			<div class="col-sm-4">
				<h5><?php the_sub_field('title'); ?></h5>
				<span class="step"><?php echo ++$chain_index; ?></span>
				<img src="<?php the_sub_field('image'); ?>" alt="" class="chain-image">
				<p><?php the_sub_field('text'); ?></p>
			</div>
			<?php } ?>
		</div>
		<?php endif; ?>
	</div>
</section>



<section class="one-section sus-section-4 hidden-xs">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-offset-5 col-sm-7">
				<h2 class="section-head"><?php the_field('section_4_title') ?></h2>
				<?php echo wpautop(get_field('section_4_text_1')); ?>
				<div class="section-ribbon"><?php the_field('section_4_ribbon_text'); ?></div>
				<?php echo wpautop(get_field('section_4_text_2')); ?>
			</div>
		</div>
		<img src="<?php the_field('section_4_image'); ?>" alt="" class="section-image">
	</div>
</section>



<section class="one-section sus-section-5 hidden-xs">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-8">
				<h2 class="section-head"><?php the_field('section_5_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_5_subtitle') ?></h3>
				<?php echo wpautop(get_field('section_5_text')); ?>

				<h5 class="section-head video-head">View Video</h5>
				<div class="row">
					<div class="col-sm-6">
						<?php echo do_shortcode('[video_lightbox_youtube video_id="' . get_field('section_5_video_link') . '" width="640" height="480" anchor="' . get_field('section_5_video_image') . '"]'); ?>
						<!-- <a href="<?php the_field('section_5_video_link'); ?>">
						<img class="video-image" src="<?php the_field('section_5_video_image'); ?>" alt="">
						</a> -->
					</div>
					<div class="col-sm-6">
						<h4 class="section-subhead3"><?php the_field('section_5_video_title'); ?></h4>
						<div class="videotext"><?php echo wpautop(get_field('section_5_video_text')); ?></div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<img src="<?php the_field('section_5_image'); ?>" alt="">
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<h3 class="toptab">Download pdf and web links</h3>

				<div class="clear"></div>

				<div class="arrow-section-h-wrap">
					<?php
					$section5List_index = 1;
					while (have_rows('section_5_list_items')): the_row(); ?>
					<section class="arrow-section-h">
						<div class="a-title">
							<h1><?php the_sub_field('title') ?></h1>
						</div>
						<div class="a-arrow">
							<span class="arrow-count"><?php echo $section5List_index++; ?></span>
						</div>
						<div class="a-content"><p><?php the_sub_field('text'); ?></p>
							<a class="a-link" href="<?php the_sub_field('link'); ?>"><?php the_sub_field('link_text'); ?></a>
						</div>
					</section>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>
</section>



<div class="hidden-xs"><?php echo do_shortcode('[sus_calculator]'); ?></div>

<?php endwhile; ?>


