<article <?php post_class('post-summary'); ?>>
	<?php get_template_part('templates/entry-meta'); ?>
  <header>
    <h2 class="entry-title"><?php the_title(); ?></h2>
  </header>
  <div class="entry-summary">
    <?php the_content(); ?>
  </div>
</article>
