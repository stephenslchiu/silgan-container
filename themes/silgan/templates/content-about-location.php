<?php while (have_posts()) : the_post(); ?>


<section class="one-section loc-section-1">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-4">
				<h1 class="section-head"><?php the_field('section_1_title'); ?></h1>
				<h2 class="section-subhead"><?php the_field('section_1_subtitle'); ?></h2>
				<?php echo wpautop(get_field('section_1_text')) ?>
			</div>
			<div class="col-sm-8">
				<?php $locations = get_field('locations');
				if ($locations) {
					echo '<img id="map-image" class="alignright" src="' . $locations[1]['map_image'] . '" alt="">';
				}
				?>
			</div>
		</div>

		<div class="panel-group" id="loc-accordion">
		<?php foreach ($locations as $il => $loc) { ?>
			<div class="panel">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#loc-accordion" href="#collapse<?php echo $il; ?>">
							<?php echo $loc['title']; ?>
						</a>
					</h4>
				</div>
				<div id="collapse<?php echo $il; ?>" data-map-image="<?php echo $loc['map_image']; ?>" class="panel-collapse collapse <?php echo ($il == 1 ? 'in' : ''); ?>">
					<div class="panel-body">
						<div class="row">
							<?php foreach ($loc['places'] as $pindex => $pla): ?>
							<div class="col-sm-4 place-panel">
								<h5 class="section-subhead4"><?php echo $pla['title']; ?></h5>
								<p><?php echo $pla['address']; ?></p>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>


		</div>
	</div>
</section>




<section class="one-section loc-section-2">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-6">
				<h2 class="section-head"><?php the_field('section_2_title'); ?></h2>
				<?php echo wpautop(get_field('section_2_text')) ?>

				<ul class="list-items">
				<?php while (have_rows('section_2_list_items')) { the_row();
					echo '<li><a href="' . get_sub_field('link') . '">' . get_sub_field('text') . '</a></li>';
				} ?>
				</ul>
			</div>
		</div>
		<img src="<?php the_field('section_2_image'); ?>" alt="" class="section-image">
	</div>
</section>




<section class="one-section loc-section-3">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6">
				<h2 class="section-head"><?php the_field('section_3_title'); ?></h2>
				<div class="contentfirst"><?php echo wpautop(get_field('section_3_text')) ?></div>
			</div>

			<img src="<?php the_field('section_3_image'); ?>" alt="" class="section-image">
		</div>

		<div class="row employee-list">
			<div class="col-sm-4">
			<?php $eindex = 0; while (have_rows('employees')) { the_row();
				if ($eindex % 2 == 0 && $eindex != 0) echo '</div><div class="col-sm-4">';
			?>
				<div class="employee">
					<h4><?php the_sub_field('name'); ?></h4>
					<h5><?php the_sub_field('title'); ?></h5>
					<div class="avatar"><img src="<?php the_sub_field('image'); ?>" alt="">
						<a href="#">See More</a>
					</div>
					<div class="description"><?php the_sub_field('description'); ?></div>
				</div>
			<?php
				$eindex++;
			} ?>
			</div>
		</div>
	</div>
</section>



<section class="one-section loc-section-4">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-6">
				<h2 class="section-head"><?php the_field('section_4_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_4_subtitle'); ?></h3>
				<?php echo wpautop(get_field('section_4_text')) ?>

				<div class="contentlink">
					<p>
					<?php while (have_rows('link_list')) { the_row();
						echo '<a href="' . get_sub_field('link') . '">' . get_sub_field('text') . '</a><br>';
					} ?>
					</p>
				</div>
			</div>
		</div>

		<img src="<?php the_field('section_4_image'); ?>" alt="" class="section-image">
	</div>
</section>


<?php endwhile; ?>