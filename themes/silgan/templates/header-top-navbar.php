<header class="banner navbar navbar-default navbar-static-top" role="banner">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo home_url(); ?>/"><?php
			if (get_field('logo', 'options')) {
				echo wp_get_attachment_image(get_field('logo', 'options'), 'full');
			} else {
				bloginfo('name');
			}
			?></a>

			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
			</button>
		</div>

		<nav class="collapse navbar-collapse" role="navigation">
			<?php
				if (has_nav_menu('primary_navigation')) :
					wp_nav_menu(array(
						'theme_location' => 'primary_navigation',
						'menu_class' => 'nav navbar-nav',
						'depth' => 3,
					));
				endif;
			?>
		</nav>
	</div>
</header>
