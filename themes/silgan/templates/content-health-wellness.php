<?php while (have_posts()) : the_post(); ?>


<?php get_template_part('templates/page', 'section-nav'); ?>



<section class="one-section health-section-1 hidden-xs">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-4">
				<h1 class="section-title"><?php the_field('section_1_title'); ?></h1>
				<h2 class="section-subhead3"><?php the_field('section_1_subtitle'); ?></h2>
				<?php echo wpautop(get_field('section_1_text_1')); ?>
				<div class="section-ribbon"><?php the_field('section_1_ribbon_text'); ?></div>
				<?php echo wpautop(get_field('section_1_text_2')); ?>
			</div>
		</div>

		<img src="<?php the_field('section_1_image'); ?>" alt="" class="section-image">
	</div>
</section>




<section class="one-section health-section-2">
	<div class="section-wrap">
		<h2 class="section-head"><?php the_field('section_2_title'); ?></h2>
		<h3 class="section-subhead"><?php the_field('section_2_subtitle'); ?></h3>
		
		<div class="row contentfirst">
			<div class="col-sm-6">
				<?php echo wpautop(get_field('section_2_text_1')); ?>
			</div>
			<div class="col-sm-5 col-sm-offset-1">
				<b><?php the_field('section_2_text_2'); ?></b>
			</div>
		</div>
	</div>

	<?php mytooltip(get_field('section_2_image')); ?>
</section>




<section class="one-section health-section-3">
	<div class="section-wrap">
		<div class="row">
			
			<div class="col-sm-7 col-sm-push-5">
				<h2 class="section-head"><?php the_field('section_3_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_3_subtitle_1'); ?></h3>
				<?php echo wpautop(get_field('section_3_text_1')); ?>
				<h3 class="section-subhead"><?php the_field('section_3_subtitle_2'); ?></h3>
				<?php echo wpautop(get_field('section_3_text_2')); ?>
			</div>
			<div class="col-sm-5 col-sm-pull-7">
				<img class="section-image" src="<?php the_field('section_3_image'); ?>" alt="">
			</div>
		</div>

		<div class="can-title"><?php the_field('section_3_can_title'); ?></div>

		<div class="can-section">
		<?php while (have_rows('section_3_can_images')) {
			the_row();
		?>
			<div class="can-item"><img src="<?php the_sub_field('image') ?>" alt="">
				<p><?php the_sub_field('footer_text'); ?></p>
			</div>
		<?php } ?>
		</div>

		<footer>Data provided by the Canned Food Alliance</footer>
	</div>
</section>



<section class="one-section health-section-4">
	<div class="section-wrap">
		<div class="section-content">
			<h2 class="section-head"><?php the_field('section_4_title'); ?></h2>
			<h3 class="section-subhead"><?php the_field('section_4_subtitle'); ?></h3>
			<?php echo wpautop(get_field('section_4_text')); ?>
		</div>

		<div class="section-image-wrap">
			<?php while (have_rows('health_items')) {
				the_row();
				echo '<div class="health-item">';
				echo '<h4>' . get_sub_field('title') . '</h4><p>';
				the_sub_field('text');
				echo '</p></div>';
			} ?>

			<img src="<?php the_field('section_4_image'); ?>" alt="" class="section-image">

			<a href="<?php the_field('button_link'); ?>" class="btn btn-right-corner"><?php the_field('button_text'); ?></a>
		</div>

		<footer><?php the_field('section_4_footer'); ?></footer>
	</div>
</section>

<?php endwhile; ?>