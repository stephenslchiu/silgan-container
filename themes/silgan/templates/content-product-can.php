<?php while (have_posts()) : the_post(); ?>


<?php get_template_part('templates/page', 'section-nav'); ?>



<section class="one-section can-section-1">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-9 col-sm-offset-3">
				<h2 class="section-title"><?php the_field('section_1_title'); ?></h2>
				<?php echo wpautop(get_field('section_1_text_1')); ?>
				<div class="section-ribbon">
					<div><?php echo wpautop(get_field('ribbon_text')); ?></div>
				</div>
				<div class="contentsecond"><?php echo wpautop(get_field('section_1_text_2')); ?></div>

			</div>
		</div>
		
		<img src="<?php the_field('section_1_image'); ?>" alt="" class="section-image">
	</div>
</section>





<section class="one-section can-section-2">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="section-head"><?php the_field('section_2_title'); ?></h2>
				<div class="contentfirst"><?php echo wpautop(get_field('section_2_text_1')); ?></div>
				<div class="contentsecond"><?php echo wpautop(get_field('section_2_text_2')); ?></div>

				<?php mytooltip(get_field('section_2_image')); ?>
			</div>
		</div>

		<div class="section-list">
			<h4><?php the_field('list_title'); ?></h4>
			<ol>
				<?php while (have_rows('list_items')): the_row(); ?>
				<li><?php the_sub_field('text'); ?></li>
				<?php endwhile; ?>
			</ol>
		</div>
	</div>
</section>




<section class="one-section can-section-3">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6 contentfirst">
				<h2 class="section-head"><?php the_field('section_3_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_3_subtitle'); ?></h3>
				<?php echo wpautop(get_field('section_3_text')); ?>
			</div>
			<div class="col-sm-6">
				<?php mytooltip(get_field('section_3_image_1')); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-7 col-sm-push-5">
				<?php mytooltip(get_field('section_3_image_2')); ?>

				<p class="clear buttonfield">
					<a href="#" class="btn btn-right-corner">Available in multiple colors</a>
				</p>
			</div>
			<div class="col-sm-5 col-sm-pull-7">
				<div class="section-list">
					<h4><?php the_field('section_3_list_title'); ?></h4>
					<ol>
						<?php while (have_rows('section_3_list_items')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ol>
				</div>
			</div>
		</div>
	</div>
</section>



<section class="one-section can-section-4 hidden-xs">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-7">
				<?php mytooltip(get_field('section_4_image')); ?>
			</div>
			<div class="col-sm-5">
				<h2 class="section-head"><?php the_field('section_4_title'); ?></h2>
				<?php echo wpautop(get_field('section_4_text')); ?>

				<div class="section-list">
					<h4><?php the_field('section_4_list_title'); ?></h4>
					<ol>
						<?php while (have_rows('section_4_list_items')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ol>
				</div>
			</div>
		</div>
	</div>
</section>



<section class="one-section can-section-5 hidden-xs">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-5">
				<h2 class="section-head"><?php the_field('section_5_title'); ?></h2>
				<?php echo wpautop(get_field('section_5_text')); ?>

				<h2 class="section-subhead2"><?php the_field('section_5_title_2'); ?></h2>
				<?php echo wpautop(get_field('section_5_text_2')); ?>
			</div>
			<div class="col-sm-7">
				<?php mytooltip(get_field('section_5_image')); ?>

				<div class="bracket-list clear">
					<ul>
						<?php while (have_rows('section_5_list_items')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="one-section can-section-6 hidden-xs">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6">
				<img src="<?php the_field('section_6_image'); ?>" alt="" class="section-image">
			</div>
			<div class="col-sm-6">
				<h2 class="section-head"><?php the_field('section_6_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_6_subtitle'); ?></h3>
				<?php echo wpautop(get_field('section_6_text')); ?>
			</div>
		</div>

		<h4 class="toptab">Advantages of Color</h4>
		<div class="row arrow-section">
			<?php while (have_rows('section_6_list_items')): the_row(); ?>
			<section class="col-sm-4">
				<h1><?php the_sub_field('title') ?></h1>
				<p><?php the_sub_field('text') ?></p>
			</section>
			<?php endwhile; ?>
		</div>
	</div>
</section>


<section class="one-section can-section-7 hidden-xs">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-7">
				<h2 class="section-head"><?php the_field('section_7_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_7_subtitle'); ?></h3>
				<?php echo wpautop(get_field('section_7_text')); ?>

				<h4 class="toptab">Advantages of Shapes</h4>
			</div>
			<div class="col-sm-5">
				<?php mytooltip(get_field('section_7_image')); ?>
			</div>
		</div>

		<div class="arrow-section-h-wrap">
			<?php
			$section7List_index = 1;
			while (have_rows('section_7_list_items')): the_row(); ?>
			<section class="arrow-section-h">
				<div class="a-title">
					<h1><?php the_sub_field('title') ?></h1>
				</div>
				<div class="a-arrow">
					<span class="arrow-count"><?php echo $section7List_index++; ?></span>
				</div>
				<div class="a-content"><p><?php the_sub_field('text'); ?></p></div>
			</section>
			<?php endwhile; ?>
		</div>
	</div>
</section>



<section class="one-section can-section-8 hidden-xs">
	<div class="section-wrap">
		<h4><?php the_field('section_8_title_1'); ?></h4>
		<div class="row">
			<div class="col-sm-7">
				<h3><?php the_field('section_8_title_2'); ?></h3>
				<?php echo wpautop(get_field('section_7_text')); ?>
			</div>
			<div class="col-sm-5">
				<div class="section-ribbon"><p><?php the_field('section_8_ribbon_text'); ?></p></div>
			</div>
		</div>
	</div>
</section>


<section class="one-section can-section-9 hidden-xs">
	<iframe src="<?php echo get_stylesheet_directory_uri() . '/tools/build-a-can/'; ?>" width="100%" height="500" frameborder="0" scrolling="no"></iframe>
</section>


<?php endwhile; ?>
