<?php while (have_posts()) : the_post(); ?>

<section class="one-section careers-section-1">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-7">
				<h2 class="section-title"><?php the_field('section_1_title'); ?></h2>
				<h4 class="section-subhead"><?php the_field('section_1_subtitle'); ?></h4>
				<?php echo wpautop(get_field('section_1_text_1')); ?>
				<div class="section-ribbon">
					<div>
						<?php echo wpautop(get_field('section_1_ribbon_text')); ?>
						<p class="subscript">— Silgan Leadership</p>
					</div>
				</div>
				<?php echo wpautop(get_field('section_1_text_2')); ?>
			</div>
			<div class="col-sm-5">
				<img src="<?php the_field('section_1_image'); ?>" alt="" class="section-image">
			</div>
		</div>
	</div>
</section>


<section class="one-section careers-section-2">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-3">
				<img src="<?php the_field('section_2_image'); ?>" alt="" class="section-image">
			</div>
			<div class="col-sm-9">
				<h2 class="section-head"><?php the_field('section_2_title'); ?></h2>
					<?php echo wpautop(get_field('section_2_text')); ?>
			</div>

		</div>
	</div>
</section>

<section class="one-section careers-section-3">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6">
				<h2 class="section-head"><?php the_field('section_3_title'); ?></h2>
				<h4 class="section-subhead"><?php the_field('section_3_subtitle'); ?></h4>
					<?php echo wpautop(get_field('section_3_text_1')); ?>
			</div>
			<div class="col-sm-6">
				<img src="<?php the_field('section_3_image_1'); ?>" alt="" class="section-image">
				<div class="section-ribbon">
					<div>
						<?php echo wpautop(get_field('section_3_ribbon_text')); ?>
						<p class="subscript">— Silgan Leadership</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<img src="<?php the_field('section_3_image_2'); ?>" alt="" class="section-image-2">
			</div>
			<div class="col-sm-6">
				<div class="section-list">
					<ol>
						<?php while (have_rows('section_3_list_items')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ol>
				</div>
				<?php echo wpautop(get_field('section_3_text_2')); ?>
			</div>
		</div>
	</div>
</section>


<section class="one-section careers-section-4">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6">
				<h2 class="section-head"><?php the_field('section_4_title'); ?></h2>
				<ul>
					<?php while (have_rows('section_4_subtitles')): the_row(); ?>
					<li><h4 class="section-subhead"><?php the_sub_field('text'); ?></h4></li>
					<?php endwhile; ?>
				</ul>
				<?php echo wpautop(get_field('section_4_text_1')); ?>
			</div>
			<div class="col-sm-6">
				<img src="<?php the_field('section_4_image'); ?>" alt="" class="section-image">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<h2><?php the_field('section_4_subtitle_1'); ?></h2>
				<ul>
					<?php while (have_rows('section_4_list_items')): the_row(); ?>
					<li><?php the_sub_field('text'); ?></li>
					<?php endwhile; ?>
				</ul>
			</div>
			<div class="col-sm-6">
				<h2><?php the_field('section_4_subtitle_2'); ?></h2>
				<?php echo wpautop(get_field('section_4_text_2')); ?>
				<h2><?php the_field('section_4_subtitle_3'); ?></h2>
				<?php echo wpautop(get_field('section_4_text_3')); ?>
			</div>
		</div>
	</div>
</section>


<section class="one-section careers-section-5">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6">
				<h2 class="section-head">Current Openings</h2>
				<h4 class="section-subhead">Quick Job Search</h4>
			</div>
			<div class="col-sm-6">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<p>Your turn</p>
			</div>
			<div class="col-sm-6">
				<h3>More Search Options</h3>
				<ul>
					<li>Advanced Job Search</li>
					<li>View All Jobs</li>
				</ul>
				<p><a href="#">To create a Job Search Agent</a>, search our careers and save your search criteria as a <a href="#">Job Search Agent</a>. It will then do the searching for you and email those jobs to you. View your current Job Agents.</p>
				<h3>Multiple Selection Tips</h3>
				<ul>
					<li>To select multiple preferences using a PC, hold down the 'Control' key while clicking the option with the mouse. To select a range, hold the shift key and click on the first and last options.</li>
					<li>Use the 'Command' key if you are using a Macintosh.</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="one-section careers-section-6">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6">
				<h2 class="section-head">Career Events Calendar</h2>
				<p>Silgan Containers participates in technical, professional and military recruiting events and college career job fairs across the country. Review the list of upcoming events and locations below and contact HR for any additional information on participation:</p>
				<h3>No events scheduled at this time.</h3>
				<p>Please check back again soon.</p>
			</div>
			<div class="col-sm-6">
				<iframe src="<?php echo tribe_get_events_link(); ?>" width="100%" frameborder="0"></iframe>
			</div>
		</div>
	</div>
</section>






















<?php endwhile; ?>
