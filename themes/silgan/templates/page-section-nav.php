<?php if (have_rows('top_nav')): ?>

<nav class="one-section">
	<div class="top-nav-carousel">
		<ul class="slides">
			<?php while (have_rows('top_nav')): the_row(); ?>
			<li>
				<a href="<?php the_sub_field('link'); ?>">
					<img src="<?php the_sub_field('image'); ?>" alt="">
					<span class="nav-title"><?php the_sub_field('title'); ?></span>
				</a>
			</li>
			<?php endwhile; ?>
		</ul>
	</div>
</nav>

<?php endif; ?>