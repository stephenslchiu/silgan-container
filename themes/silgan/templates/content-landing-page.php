<?php while (have_posts()) : the_post(); ?>
<article class="hentry landing-entry">
	<header>
		<h1 class="section-title"><?php the_title(); ?></h1>
	</header>
	<div class="entry-content"><?php the_content(); ?></div>
	
	
	<?php if (get_field('navigation_slider')): ?>
	<footer>
		<div class="carouselslider">
			<ul class="slides">
				<?php while (have_rows('navigation_slider')): the_row(); ?>
				<li><a href="<?php the_sub_field('page'); if (get_sub_field('hash_section')) echo '#' . get_sub_field('hash_section'); ?>"><?php
					echo wp_get_attachment_image(get_sub_field('image'), 'full');
				?></a></li>
				<?php endwhile; ?>
			</ul>
		</div>
	</footer>
	<?php endif; ?>
</article>
<?php endwhile; ?>
