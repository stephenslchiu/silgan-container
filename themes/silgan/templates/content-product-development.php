<?php while (have_posts()) : the_post(); ?>


<?php get_template_part('templates/page', 'section-nav'); ?>




<section class="one-section dev-section-1 hidden-xs">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-4">
				<h1 class="section-title"><?php the_field('section_1_title'); ?></h1>
				<?php echo wpautop(get_field('section_1_text')); ?>
				<div class="section-ribbon"><?php the_field('section_1_ribbon_text'); ?></div>
			</div>
		</div>
		<img src="<?php the_field('section_1_image'); ?>" alt="" class="section-image">
	</div>
</section>






<section class="one-section dev-section-2">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6">
				<h2 class="section-head"><?php the_field('section_2_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_2_subtitle'); ?></h3>
				<?php echo wpautop(get_field('section_2_text_1')); ?>
			</div>

			<img src="<?php the_field('section_2_image'); ?>" alt="" class="section-image">
		</div>

		<div class="dev-table-header">
			<div class="row">
				<div class="col-sm-4">Determine Your Goals</div>
				<div class="col-sm-8">Silgan Develop Your Solution</div>
			</div>
		</div>
		<div class="dev-table">
			<div class="row">
			<?php while (have_rows('section_2_development_table')) { the_row(); ?>
				<div class="col-sm-2 col-xs-6 dev-table-item">
					<h4><?php the_sub_field('title') ?></h4>
					<?php echo wpautop(get_sub_field('text')); ?>
				</div>
			<?php } ?>
			</div>
		</div>

		<div class="row">
		<div class="col-sm-6"><?php echo wpautop(get_field('section_2_text_2')); ?></div>
		</div>
	</div>
</section>





<section class="one-section dev-section-3">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6">
				<h2 class="section-head"><?php the_field('section_3_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_3_subtitle'); ?></h3>
				<?php echo wpautop(get_field('section_3_text')); ?>
			</div>
		</div>
	</div>

	<style>
	.one-section.dev-section-3 {
		background-image: url(<?php the_field('section_3_image'); ?>);
	}
	</style>
</section>






<section class="one-section dev-section-4 hidden-xs">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-7">
				<div class="list-items">
					<div class="row">
					<?php while (have_rows('section_4_list_items')) { the_row(); ?>
						<div class="col-sm-6 item">
							<div class="row">
								<div class="col-xs-5">
									<img src="<?php the_sub_field('image'); ?>" alt="">
								</div>
								<div class="col-xs-7">
									<h5 class="section-subhead4"><?php the_sub_field('title'); ?></h5>
									<?php echo wpautop(get_sub_field('text')); ?>
								</div>
							</div>
						</div>
					<?php } ?>
					</div>
				</div>
			</div>
			<div class="col-sm-5">
				<h2 class="section-head"><?php the_field('section_4_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_4_subtitle'); ?></h3>
				<?php echo wpautop(get_field('section_4_text')); ?>

				<div class="section-ribbon"><?php the_field('section_4_ribbon_text'); ?></div>
			</div>
		</div>
	</div>
</section>






<section class="one-section dev-section-5">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-5">
				<h2 class="section-head"><?php the_field('section_5_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_5_subtitle'); ?></h3>
				<?php echo wpautop(get_field('section_5_text')); ?>

				<h3 class="section-subhead3"><?php the_field('section_5_title_2'); ?></h3>
				<?php echo wpautop(get_field('section_5_text_2')); ?>






				<div class="product-development-graph">
					<ul class="slides">
					<?php for ($i = 1; $i <= 10; $i++) { 
						echo '<li><img src="' . get_stylesheet_directory_uri() . '/assets/img/product-development-graph/State-' . str_pad($i, 2, '0', STR_PAD_LEFT) . '.png" alt=""></li>';
					} ?>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<style>
	.one-section.dev-section-5 {
		background: url(<?php the_field('section_5_image'); ?>) right bottom no-repeat, url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/supp-bg.png) 0 0 repeat-x;
		background-color: #fbfefe;
	}
	</style>
</section>



<?php endwhile; ?>
