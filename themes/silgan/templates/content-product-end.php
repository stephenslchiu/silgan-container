<?php while (have_posts()) : the_post(); ?>


<?php get_template_part('templates/page', 'section-nav'); ?>



<section class="one-section end-section-1">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-8">
				<h2 class="section-title"><?php the_field('section_1_title'); ?></h2>

				<div class="contentfirst"><?php echo wpautop(get_field('section_1_text')); ?></div>

				<div class="section-ribbon">
					<div><?php echo wpautop(get_field('section_1_ribbon_text')); ?></div>
				</div>
			</div>
		</div>
		
		<img src="<?php the_field('section_1_image'); ?>" alt="" class="section-image">
	</div>
</section>


<section class="one-section end-section-2">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-12 col-lg-10 col-lg-offset-1">
				<h2 class="section-head"><?php the_field('section_2_title'); ?></h2>
				<?php echo wpautop(get_field('section_2_text')); ?>
			</div>
		</div>
		<?php
		$image_left = get_field('section_2_image_left');
		$image_right = get_field('section_2_image_right');
		?>
		<div class="row section-image-wrap">
			<div class="col-sm-6">
				<h1><?php the_field('section_2_image_title_left'); ?></h1>
				<?php mytooltip($image_left); ?>

				<div class="bracket-list">
					<div class="bracket-content">
						<?php echo wpautop($image_left['description']); ?>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<h1><?php the_field('section_2_image_title_right'); ?></h1>
				<?php mytooltip($image_right); ?>

				<div class="bracket-list">
					<div class="bracket-content">
						<?php echo wpautop($image_right['description']); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="one-section end-section-3">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-7">
				<h2 class="section-head"><?php the_field('section_3_title'); ?></h2>
				<?php echo wpautop(get_field('section_3_text_1')); ?>
			</div>
			<div class="col-sm-5">
				<?php mytooltip(get_field('section_3_image_1'), array( 'class' => 'alignright' )); ?>
				<!-- <img src="<?php the_field('section_3_image_1'); ?>" alt="" class="section-image alignright"> -->
			</div>
		</div>
		<div class="row flex contentsecond">
			<div class="col-sm-3">
				<?php mytooltip(get_field('section_3_image_2')); ?>
			</div>
			<div class="col-sm-9 flex flexaligncenter">
				<b><?php the_field('section_3_text_2'); ?></b>
			</div>
		</div>
	</div>
</section>


<?php endwhile; ?>
