<?php while (have_posts()) : the_post(); ?>
	<h1 class="section-head"><?php the_title(); ?></h1>

	<nav class="sitemap">
		<div class="row">
		<?php
		$theme_locations = get_nav_menu_locations();
		$sitemap_id = $theme_locations['sitemap_navigation'];
		$sitemap_items = wp_get_nav_menu_items($sitemap_id);
		$sindex = 0;
		$precol = false;

		foreach ($sitemap_items as $sitem) {
			if ($sindex == 0 && $sitem->menu_item_parent == 0) {
				echo '<div class="col-sm-4">';
			} else if ($sitem->menu_item_parent == 0 && ($sindex == 2 || $sindex == 4)) echo '</div><div class="col-sm-4">';

			if ($sindex == 4) $precol = true;
			
			if ($sitem->menu_item_parent == 0) $sindex++;

			if ($sitem->menu_item_parent == 0 && !$precol) {
				echo '<h3><a href="#">' . $sitem->title . '</a></h3>';
			} else echo '<p><a href="#">' . $sitem->title . '</a></p>';
			?>
		<?php
		} ?>
		</div>
	</nav>
	<?php the_content(); ?>
<?php endwhile; ?>
