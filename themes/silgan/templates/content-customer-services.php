<?php while (have_posts()) : the_post(); ?>


<nav class="one-section"></nav>



<section class="one-section customer-section-1">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-7 col-sm-offset-5">
				<h2 class="section-title"><?php the_field('section_1_title'); ?></h2>
				<h4 class="section-subhead4"><?php the_field('section_1_subtitle'); ?></h4>
				<?php echo wpautop(get_field('section_1_text_1')); ?>
				<div class="section-ribbon">
					<div><?php echo wpautop(get_field('ribbon_text')); ?></div>
				</div>
				<div class="section-point-list">
				<ul>
					<?php while (have_rows('section_1_list_items')): the_row(); ?>
					<li><?php the_sub_field('text'); ?></li>
					<?php endwhile; ?>
				</ul>
				</div>
				<?php echo wpautop(get_field('section_1_text_2')); ?>
			</div>
		</div>
	<div>
		<img src="<?php the_field('section_1_image'); ?>" alt="" class="section-image">
	</div>
</section>



<section class="one-section customer-section-2">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6">
				<h2 class="section-head"><?php the_field('section_2_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_2_subtitle_1'); ?></h3>
				<?php echo wpautop(get_field('section_2_text')); ?>
				<h3 class="section-subhead"><?php the_field('section_2_subtitle_2'); ?></h3>
				<h4 class="section-subhead4"><?php the_field('section_2_list_title_1'); ?></h4>
				<ul>
					<?php while (have_rows('section_2_list_items_1')): the_row(); ?>
					<li><?php the_sub_field('text'); ?></li>
					<?php endwhile; ?>
				</ul>
				<h4 class="section-subhead4"><?php the_field('section_2_list_title_2'); ?></h4>
				<ul>
					<?php while (have_rows('section_2_list_items_2')): the_row(); ?>
					<li><?php the_sub_field('text'); ?></li>
					<?php endwhile; ?>
				</ul>
				<h4 class="section-subhead4"><?php the_field('section_2_list_title_3'); ?></h4>
				<ul>
					<?php while (have_rows('section_2_list_items_3')): the_row(); ?>
					<li><?php the_sub_field('text'); ?></li>
					<?php endwhile; ?>
				</ul>
				<h4 class="section-subhead4"><?php the_field('section_2_list_title_4'); ?></h4>
				<ul>
					<?php while (have_rows('section_2_list_items_4')): the_row(); ?>
					<li><?php the_sub_field('text'); ?></li>
					<?php endwhile; ?>
				</ul>
				<h4 class="section-subhead4"><?php the_field('section_2_list_title_5'); ?></h4>
				<ul>
					<?php while (have_rows('section_2_list_items_5')): the_row(); ?>
					<li><?php the_sub_field('text'); ?></li>
					<?php endwhile; ?>
				</ul>
			</div>
			<div class="col-sm-6">
				<img src="<?php the_field('section_2_image'); ?>" alt="" class="section-image">
			</div>
		</div>
	</div>
</section>


<section class="one-section customer-section-3">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-5">
				<img src="<?php the_field('section_3_image_1'); ?>" alt="" class="section-image">
				<img src="<?php the_field('section_3_image_2'); ?>" alt="" class="section-image">
				<img src="<?php the_field('section_3_image_3'); ?>" alt="" class="section-image">
			</div>
			<div class="col-sm-7">
				<h2 class="section-head"><?php the_field('section_3_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_3_subtitle_1'); ?></h3>
				<?php echo wpautop(get_field('section_3_text')); ?>
				<h3 class="section-subhead"><?php the_field('section_3_subtitle_2'); ?></h3>
					<h4 class="section-subhead4"><?php the_field('section_3_list_title_1'); ?></h4>
					<ul>
						<?php while (have_rows('section_3_list_items_1')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ul>
					<h4 class="section-subhead4"><?php the_field('section_3_list_title_2'); ?></h4>
					<ul>
						<?php while (have_rows('section_3_list_items_2')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ul>
					<h4 class="section-subhead4"><?php the_field('section_3_list_title_3'); ?></h4>
					<ul>
						<?php while (have_rows('section_3_list_items_3')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ul>
					<h4 class="section-subhead4"><?php the_field('section_3_list_title_4'); ?></h4>
					<ul>
						<?php while (have_rows('section_3_list_items_4')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ul>
					<h4 class="section-subhead4"><?php the_field('section_3_list_title_5'); ?></h4>
					<ul>
						<?php while (have_rows('section_3_list_items_5')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ul>
			</div>
		</div>
	</div>
</section>


<section class="one-section customer-section-4">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6">
				<h2 class="section-head"><?php the_field('section_4_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_4_subtitle_1'); ?></h3>
				<?php echo wpautop(get_field('section_4_text')); ?>
				<h3 class="section-subhead"><?php the_field('section_4_subtitle_2'); ?></h3>
				<h4 class="section-subhead4"><?php the_field('section_4_list_title_1'); ?></h4>
				<ul>
					<?php while (have_rows('section_4_list_items_1')): the_row(); ?>
					<li><?php the_sub_field('text'); ?></li>
					<?php endwhile; ?>
				</ul>
				<h4 class="section-subhead4"><?php the_field('section_4_list_title_2'); ?></h4>
				<ul>
					<?php while (have_rows('section_4_list_items_2')): the_row(); ?>
					<li><?php the_sub_field('text'); ?></li>
					<?php endwhile; ?>
				</ul>
				<h4 class="section-subhead4"><?php the_field('section_4_list_title_3'); ?></h4>
				<ul>
					<?php while (have_rows('section_4_list_items_3')): the_row(); ?>
					<li><?php the_sub_field('text'); ?></li>
					<?php endwhile; ?>
				</ul>
				<h4 class="section-subhead4"><?php the_field('section_4_list_title_4'); ?></h4>
				<ul>
					<?php while (have_rows('section_4_list_items_4')): the_row(); ?>
					<li><?php the_sub_field('text'); ?></li>
					<?php endwhile; ?>
				</ul>
			</div>
			<div class="col-sm-6">
				<img src="<?php the_field('section_4_image'); ?>" alt="" class="section-image">
			</div>
		</div>
	</div>
</section>


<section class="one-section customer-section-5">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-6">
				<img src="<?php the_field('section_5_image_1'); ?>" alt="" class="section-image">
				<h3 class="image-title"><?php the_field('section_5_image_title'); ?></h3>
				<img src="<?php the_field('section_5_image_2'); ?>" class="section-image">
			</div>
			<div class="col-sm-6">
				<h2 class="section-head"><?php the_field('section_5_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_5_subtitle_1'); ?></h4>
				<?php echo wpautop(get_field('section_5_text')); ?>
				<h3 class="section-subhead"><?php the_field('section_5_subtitle_2'); ?></h4>
					<h4 class="section-subhead4"><?php the_field('section_5_list_title_1'); ?></h4>
					<ul>
						<?php while (have_rows('section_5_list_items_1')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ul>
					<h4 class="section-subhead4"><?php the_field('section_5_list_title_2'); ?></h4>
					<ul>
						<?php while (have_rows('section_5_list_items_2')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ul>
				<div class="section-ribbon">
					<div>
						<?php echo wpautop(get_field('section_5_ribbon_text')); ?>
					</div>
				</div>
					<h4 class="section-subhead4"><?php the_field('section_5_list_title_3'); ?></h4>
					<ul>
						<?php while (have_rows('section_5_list_items_3')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ul>
					<h4 class="section-subhead4"><?php the_field('section_5_list_title_4'); ?></h4>
					<ul>
						<?php while (have_rows('section_5_list_items_4')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ul>
					<h4 class="section-subhead4"><?php the_field('section_5_list_title_5'); ?></h4>
					<ul>
						<?php while (have_rows('section_5_list_items_5')): the_row(); ?>
						<li><?php the_sub_field('text'); ?></li>
						<?php endwhile; ?>
					</ul>
			</div>
		</div>
	</div>
</section>


<?php endwhile; ?>
