<p>
	<time class="published" datetime="<?php echo get_the_time('c'); ?>"><?php echo get_the_date('M j, Y'); ?></time>
</p>
