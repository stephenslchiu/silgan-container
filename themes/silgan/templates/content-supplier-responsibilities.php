	<?php while (have_posts()) : the_post(); ?>

<section class="one-section supplier-section-1">
	<div class="section-wrap">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="section-head"><?php the_field('section_1_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_1_subtitle'); ?></h3>
				<?php echo wpautop(get_field('section_1_text')); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-5 hidden-xs">
				<img src="<?php the_field('section_2_image_1'); ?>" alt="" class="section-image">
				<img src="<?php the_field('section_2_image_2'); ?>" alt="" class="section-image">
				<img src="<?php the_field('section_2_image_3'); ?>" alt="" class="section-image">
			</div>
			<div class="col-sm-7">
				<h2 class="section-head"><?php the_field('section_2_title'); ?></h2>
				<h3 class="section-subhead"><?php the_field('section_2_subtitle'); ?></h3>
				<?php echo wpautop(get_field('section_2_text')); ?>
			</div>
		</div>
	</div>
</section>


<?php endwhile; ?>
