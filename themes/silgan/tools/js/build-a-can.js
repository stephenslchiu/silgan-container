jQuery(document).ready(function () {
	function repositionImage () {
		$('.can-image img').each(function () {
			$(this).css({
				marginLeft: function () {
					return -($(this).width() / 2);
				},
				marginTop: function () {
					return -($(this).height() / 2);
				}
			})
		});
	}

	$('#can-image, #lid-image').load(function () {
		repositionImage();
	}).each(function() {
		if(this.complete) $(this).load();
	});



	$('.select-type').on('mouseenter', 'a', function () {
		var $img = $(this).find('img');
		$img.data('orig', $img.attr('src'));
		$img.attr('src', $img.data('over'));
	}).on('mouseleave', 'a', function () {
		var $img = $(this).find('img');
		$img.attr('src', $img.data('orig'));
	}).on('click', 'a', function (e) {
		$('input[name="' + $(e.delegateTarget).data('type') + '_type"]').val($(this).data('typeIndex'));
		refreshImages();
		e.preventDefault();
	});

	$('.select-color').on('click', 'a', function (e) {
		$('input[name="' + $(e.delegateTarget).data('type') + '_color"]').val($(this).data('color'));
		refreshImages();
		e.preventDefault();
	});


	function refreshImages () {
		var can_type = $('input[name="can_type"]').val(),
			lid_type = $('input[name="lid_type"]').val();

		if (can_type.length == 1) can_type = '0' + can_type;
		if (lid_type.length == 1) lid_type = '0' + lid_type;

		$('#can-image').attr('src', imageLink.replace(/{type}/g, 'can').replace('{index}', can_type).replace('{color}', $('input[name="can_color"]').val()));
		$('#lid-image').attr('src', imageLink.replace(/{type}/g, 'lid').replace('{index}', lid_type).replace('{color}', $('input[name="lid_color"]').val()));
	}
	refreshImages();
});