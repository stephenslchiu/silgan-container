jQuery(document).ready(function ($) {
	var columns = $('.thetimeline td').length,
		fullWidth = columns * 170;

	$('.thetimeline').width(fullWidth);
	$('.timeline-bar').css({
		width: fullWidth,
		height: 40
	});
	var timelineInner = $('.timeline-inner').width(fullWidth);


	$('.timeline-control').on('click', 'a', function (e) {
		var activeTD = $('.thetimeline td.active');
		if (!activeTD.length) activeTD = $('.thetimeline td:first-child');

		var $sender = $(this),
			$comingTD = null;
		if ($sender.hasClass('next')) $comingTD = activeTD.next();
		else $comingTD = activeTD.prev();

		var rightmost = -(fullWidth - $('.timeline-content').width());

		if ($comingTD.length) {
			var toLeft = -$comingTD.position().left;
			if (toLeft < rightmost) {
				timelineInner.css('left', rightmost);
			} else {
				timelineInner.css('left', toLeft);
				activeTD.removeClass('active');
				$comingTD.addClass('active');
			}
		}
		e.preventDefault();
	});
});