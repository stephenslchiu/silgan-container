<?php
require('../../../../../wp-blog-header.php');
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Timeline</title>

		<script src="//cdn.jsdelivr.net/modernizr/2.7.1/modernizr.min.js"></script>

		<!-- Bootstrap -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="../css/timeline.css" rel="stylesheet">

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<link data-noprefix="true" href='//fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

		<script src="//cdn.jsdelivr.net/prefixfree/1.0.7/prefixfree.min.js"></script>

		<script type="text/javascript" src="//use.typekit.net/ukk1dsx.js"></script>
  	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	</head>
	<body>
		<div class="container">
			<?php if (!isset($_GET['n'])) {
				echo '<h1><span>HISTORY OF SILGAN</span></h1>';
			} else { ?>


<style>
	body { background: none transparent; }
	.container { min-height: 0; }
	.timeline { margin-top: 0; }
</style>


			<?php } ?>



			<div class="timeline">
				<div class="timeline-content">
					<div class="timeline-inner">
						<table class="thetimeline">
							<tr>
								<td class="active">
									<div class="timeline-image"><img src="img/1899.png" alt=""></div>
									<h5><span>1899</span></h5>
									<p>Carnation Company (then Pacific Coast Condensed Milk Company) opens its first self-make can operation in Kent.</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1901.png" alt=""></div>
									<h5><span>1901</span></h5>
									<p>Union Can Company, Hoopeston, IL, joins</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1935.png" alt=""></div>
									<h5><span>1935</span></h5>
									<p>National Can Company</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1936.png" alt=""></div>
									<h5><span>1936</span></h5>
									<p>Campbell's Soup company begins</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1941.png" alt=""></div>
									<h5><span>1941-45</span></h5>
									<p>Hornel Spam canned luncheon meat feeds</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1953.png" alt=""></div>
									<h5><span>1953</span></h5>
									<p>Del Monte begins manufacturing</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1969.png" alt=""></div>
									<h5><span>1969</span></h5>
									<p>Canned Carnation Spreadable' go to the moon Apollo 11</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1970.png" alt=""></div>
									<h5><span>1970</span></h5>
									<p>Carnation’s Can Division<br>
									Converts to welded side<br>
									seam technology<br>
									Introduces 2-piece</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1979.png" alt=""></div>
									<h5><span>1979</span></h5>
									<p>American Can begins commercial sales to Green Giant</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1986.png" alt=""></div>
									<h5><span>1986</span></h5>
									<p>American and National Can merge to form American National Can Company Carnation’s Can Division begins high-speed aluminum can production</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1987.png" alt=""></div>
									<h5><span>1987</span></h5>
									<p>Silgan Containers Corporation created with the purchase of Carnation's Can Division</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1988.png" alt=""></div>
									<h5><span>1988</span></h5>
									<p>Ft. Madison Can Company acquired</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1993.png" alt=""></div>
									<h5><span>1993</span></h5>
									<p>Del Monte’s metal Container manufacturing division acquired Northern Can System’s Menomonie, WI, manufacturing facility acquired</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1995.png" alt=""></div>
									<h5><span>1995</span></h5>
									<p>American National Can Co.’s Food Metal &amp; Specialty Business acquired</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1996.png" alt=""></div>
									<h5><span>1996</span></h5>
									<p>Finger Lakes Packaging Co. acquired</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1997.png" alt=""></div>
									<h5><span>1997</span></h5>
									<p>Kraft’s Tarrant, AL, plate processing facility acquired Alcoa’s aluminum closure facility acquired</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1998.png" alt=""></div>
									<h5><span>1998</span></h5>
									<p>Campbell Soup Company’s can manufacturing operations acquired</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/1999.png" alt=""></div>
									<h5><span>1999</span></h5>
									<p>Can of the Year® award from The Canmaker magazine for the Hormel Spam® 2-piece aluminum container Silgan celebrates 100th Birthday of Carnation’s Evaporated Milk can</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/2000.png" alt=""></div>
									<h5><span>2000</span></h5>
									<p>Chiquita Payette, WA, can making facility acquired</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/2003.png" alt=""></div>
									<h5><span>2003</span></h5>
									<p>Pacific Coast Producers container manufacturing facility acquired</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/2004.png" alt=""></div>
									<h5><span>2004</span></h5>
									<p>Dot Top earns “Can of the Year” honors from The Canmaker magazine</p>
								</td>
								<td>
									<div class="timeline-image"><img src="img/2011.png" alt=""></div>
									<h5><span>2011</span></h5>
									<p>Nestlé Purina Pet Care Co. steel can assets for pet food products is acquired</p>
								</td>
							</tr>
						</table>
						<img class="timeline-bar" src="<?php echo get_stylesheet_directory_uri(); ?>/tools/timeline/img/timeline.jpg" alt=""></div>
					</div>

					<div class="timeline-control">
						<a class="prev" href="#">&lt;</a>
						<a class="next" href="#">&gt;</a>
					</div>
				</div>
			</div>
		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="../js/timeline.js"></script>
	</body>
</html>