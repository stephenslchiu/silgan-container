<?php
require('../../../../../wp-blog-header.php');
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Build a can tool</title>

		<script src="//cdn.jsdelivr.net/modernizr/2.7.1/modernizr.min.js"></script>

		<!-- Bootstrap -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="../css/build-a-can.css" rel="stylesheet">

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<link data-noprefix="true" href='//fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

		<script src="//cdn.jsdelivr.net/prefixfree/1.0.7/prefixfree.min.js"></script>
		<script type="text/javascript" src="//use.typekit.net/ukk1dsx.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	</head>
	<body>
		<form action="">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1><span>Build-A-Can</span></h1>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 can-col can-type">
						<div class="can-image">
							<img id="can-image" src="img/can_01/can_red.png" alt="">
						</div>
			
						<span class="label">Choose a shape</span>
			
						<div class="select-option select-type" data-type="can">
							<a data-type-index="1" href="#"><img src="img/can_01/can_01.png" data-over="img/can_01/can_01_highlighted.png" alt=""></a>
							<a data-type-index="2" href="#"><img src="img/can_02/can_02.png" data-over="img/can_02/can_02_highlighted.png" alt=""></a>
							<a data-type-index="3" href="#"><img src="img/can_03/can_03.png" data-over="img/can_03/can_03_highlighted.png" alt=""></a>
							<a data-type-index="4" href="#"><img src="img/can_04/can_04.png" data-over="img/can_04/can_04_highlighted.png" alt=""></a>
							<a data-type-index="5" href="#"><img src="img/can_05/can_05.png" data-over="img/can_05/can_05_highlighted.png" alt=""></a>
							<a data-type-index="6" href="#"><img src="img/can_06/can_06.png" data-over="img/can_06/can_06_highlighted.png" alt=""></a>
							<a data-type-index="7" href="#"><img src="img/can_07/can_07.png" data-over="img/can_07/can_07_highlighted.png" alt=""></a>
						</div>
			
						<span class="label">Choose a color</span>
						<div class="select-option select-color" data-type="can">
							<a href="#" data-color="red" class="red">Red</a>
							<a href="#" data-color="green" class="green">Green</a>
							<a href="#" data-color="blue" class="blue">Blue</a>
							<a href="#" data-color="purple" class="purple">Purple</a>
							<a href="#" data-color="orange" class="orange">Orange</a>
						</div>
					</div>
					<div class="col-sm-6 can-col lid-type">
						<div class="can-image">
							<img id="lid-image" src="img/lid_01/lid_red.png" alt="">
						</div>
			
						<span class="label">Choose a lid</span>
			
						<div class="select-option select-type" data-type="lid">
							<a data-type-index="1" href="#"><img src="img/lid_01/lid_01.png" data-over="img/lid_01/lid_01_highlighted.png" alt=""></a>
							<a data-type-index="2" href="#"><img src="img/lid_02/lid_02.png" data-over="img/lid_02/lid_02_highlighted.png" alt=""></a>
							<a data-type-index="3" href="#"><img src="img/lid_03/lid_03.png" data-over="img/lid_03/lid_03_highlighted.png" alt=""></a>
							<a data-type-index="4" href="#"><img src="img/lid_04/lid_04.png" data-over="img/lid_04/lid_04_highlighted.png" alt=""></a>
						</div>
			
						<span class="label">Choose a color</span>
						<div class="select-option select-color" data-type="lid">
							<a href="#" data-color="red" class="red">Red</a>
							<a href="#" data-color="green" class="green">Green</a>
							<a href="#" data-color="blue" class="blue">Blue</a>
							<a href="#" data-color="purple" class="purple">Purple</a>
							<a href="#" data-color="orange" class="orange">Orange</a>
						</div>
					</div>
				</div>
			
				<p>Colors may also be applied to tabs to match or contrast ends.</p>
				<p><a href="#" class="btn btn-right-corner">I'm done</a></p>
			
				<br><br><br><br>
			</div>
			
			
			<div class="preload">
				<img src="img/can_01/can_01_highlighted.png" alt="">
				<img src="img/can_02/can_02_highlighted.png" alt="">
				<img src="img/can_03/can_03_highlighted.png" alt="">
				<img src="img/can_04/can_04_highlighted.png" alt="">
				<img src="img/can_05/can_05_highlighted.png" alt="">
				<img src="img/can_06/can_06_highlighted.png" alt="">
				<img src="img/can_07/can_07_highlighted.png" alt="">
			
				<img src="img/lid_01/lid_01_highlighted.png" alt="">
				<img src="img/lid_02/lid_02_highlighted.png" alt="">
				<img src="img/lid_03/lid_03_highlighted.png" alt="">
				<img src="img/lid_04/lid_04_highlighted.png" alt="">
			</div>

			<input type="hidden" name="can_color" value="red">
			<input type="hidden" name="can_type" value="1">
			<input type="hidden" name="lid_color" value="red">
			<input type="hidden" name="lid_type" value="1">
		</form>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		
		<script>
		var imageLink = '<?php echo get_stylesheet_directory_uri(); ?>/tools/build-a-can/img/{type}_{index}/{type}_{color}.png';
		</script>
		<script src="../js/build-a-can.js"></script>
	</body>
</html>