

(function($) {

// Use this variable to set up the common and page specific functions. If you 
// rename this variable, you will also need to rename the namespace below.
var Roots = {
	// All pages
	common: {
		init: function() {

			function carousel_itemCount () {
				if (Modernizr.mq('(max-width: 767px)')) {
					return 2;
				} else {
					return 3;
				}
			}

			var carouselslider = $('.carouselslider');
			if (carouselslider.length) {
				var icount = carousel_itemCount();

				$(window).load(function () {
					carouselslider.flexslider({
						animation: "slide",
						animationLoop: false,
						itemWidth: 200,
						itemMargin: 20,
						keyboard: false,
						controlNav: false,
						slideshow: false,
						move: 1,
						maxItems: icount,
						minItems: icount,
						directionNav: true,
					});
				}).on('resize', function () {
					api.vars.maxItems = api.vars.minItems = carousel_itemCount();
				});
			}



			var topnavcarousel = $('.top-nav-carousel');
			if (topnavcarousel.length) {
				var topnav_index = 0;

				function topnav_itemCount () {
					// var itemsCount = Math.ceil(topnavcarousel.width() / 320);
					var itemsCount = 4;
					
					if (Modernizr.mq('(max-width: 767px)')) {
						itemsCount = 2;
					} else if (Modernizr.mq('(max-width: 991px)')) {
						itemsCount = 3;
					}

					return itemsCount;
				}

				

				$(window).load(function () {
					var itemsCount = topnav_itemCount();
					topnavcarousel.flexslider({
						keyboard: false,
						animation: "slide",
						animationLoop: false,
						itemWidth: 320,
						itemMargin: 0,
						controlNav: false,
						slideshow: false,
						maxItems: itemsCount,
						minItems: itemsCount,
						move: 1,
						directionNav: false,
						start: function() {
							topnavcarousel.find('.slides li:lt(' + itemsCount + ')').addClass('in-view');

							topnavcarousel.append('<ul class="flex-direction-nav"><li><a href="#" class="flex-prev">Previous</a></li><li><a href="#" class="flex-next">Next</a></li></ul>');

							setTimeout(function () {
								$(window).trigger('resize');
							}, 1000);
						}
					}).on('click', 'a', function (e) {
						var $sender = $(this);
						if ($sender.hasClass('flex-prev')) {
							var inview = topnavcarousel.find('li.in-view'),
								coming_inview = inview.first().prev();

							if (coming_inview.length) {
								inview.last().removeClass('in-view');
								coming_inview.addClass('in-view');
								topnavcarousel.flexslider("prev");
							}
						} else if ($sender.hasClass('flex-next')) {
							var inview = topnavcarousel.find('li.in-view'),
								coming_inview = inview.last().next();

							if (coming_inview.length) {
								inview.first().removeClass('in-view');
								coming_inview.addClass('in-view');
								topnavcarousel.flexslider("next");
							}
						}
						e.preventDefault();
					});
				}).on('resize', function () {
					var api = topnavcarousel.data('flexslider');
					if (api) {
						var gridSize = topnav_itemCount();
						api.vars.minItems = gridSize;
						api.vars.maxItems = gridSize;
					}
				});
			}


			$('iframe').load(function () {
				resizeIframe(this);
			});






			$('.section_table td > div').each(function () {
				$(this).css('height', $(this).parent().height());
			});

			$('.can_title').html(function () {
				return $(this).html().replace('$', '<sup>$</sup>');
			});


			$('.product-development-graph').flexslider({
				directionNav: false,
				controlNav: false,
				slideshowSpeed: 2500
			});







			$('.tooltip-container a').each(function () {
				var option = {
					content: {
						title: $(this).attr('title'),
						text: $($(this).attr('href')),
						button: true
					},
					position: {
						my: 'left bottom',
						at: 'right bottom',
						adjust: { }
					},
					show: {
						event: 'click'
					},
					hide: {
						event: 'unfocus'
					},
					style: {
						tip: {
							corner: true,
							width: 15,
							height: 60
						}
					},
					events: { }
				};

				var mydata = $(this).data();
				if (mydata['adjustX'])
					option['position']['adjust']['x'] = mydata['adjustX'];
				if (mydata['adjustY'])
					option['position']['adjust']['y'] = mydata['adjustY'];
				if (mydata['positionMy'])
					option['position']['my'] = mydata['positionMy'];
				if (mydata['positionAt'])
					option['position']['at'] = mydata['positionAt'];
				if (mydata['tipCorner'])
					option['style']['tip']['corner'] = mydata['tipCorner'];
				if (mydata['tipWidth'])
					option['style']['tip']['width'] = mydata['tipWidth'];
				if (mydata['tipHeight'])
					option['style']['tip']['height'] = mydata['tipHeight'];
				if (mydata['cssTransform']) {
					option['events']['render'] = function (ev, api) {
						var tip = $(api.elements.tooltip).find('.qtip-tip');
						tip.attr('style', tip.attr('style') + mydata['cssTransform']);
					}
				}

				var myapi = $(this).qtip(option).qtip('api');

				// myapi.show(); 
			}).on('click', function (e) {
				e.preventDefault();
			});




			var tooltip_image_resizer = _.throttle(function() {
				$('.tooltip-container img').each(function () {
					var $img = $(this),
						$container = $img.closest('.tooltip-container'),
						owidth = Number($img.attr('width'));
					if (owidth && !($img.width() <= owidth)) {
						$container.css('display', 'block');
					} else {
						$container.css('display', '');
					}
				});
			}, 500);
			$(window).on('resize', tooltip_image_resizer);
		}
	},
	// Home page
	home: {
		init: function() {
			$('.myshare').share({
				networks: ['facebook','twitter','email']
			});
		}
	},
	// About us page, note the change from about_us to about_us.
	about_us: {
		init: function() {
			// JavaScript to be fired on the about us page
		}
	},
	locations: {
		init: function () {
			$('#loc_accordion').on('shown.bs.collapse', function (e) {
				var $sender = $(e.target);
				if ($sender.length) {
					$('#map_image').attr('src', $sender.data('mapImage'));
				}
			});

			$('.employee').on('click', '.avatar a', function (e) {
				$(e.delegateTarget).find('.description').toggle();
				var $sender = $(this);
				if ($sender.text().toLowerCase() == 'see more') $sender.text('close');
				else $sender.text('see more');
				e.preventDefault();
			});
		}
	},
	events_archive: {
		init: function () {
			$('#tribe_events_content td a').click(function () {
				return false;
			});
		}
	},
	silgan_u: {
		init: function () {
			$('.landing-entry header h1').html(function () {
				var headertext = $(this).text();
				return headertext.replace('U', '<span class="silgan-big-u">U</span>');
			});
		}
	}
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
	fire: function(func, funcname, args) {
		var namespace = Roots;
		funcname = (funcname === undefined) ? 'init' : funcname;
		if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
			namespace[func][funcname](args);
		}
	},
	loadEvents: function() {
		UTIL.fire('common');

		$.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
			UTIL.fire(classnm);
		});
	}
};

$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.



function resizeIframe(obj) {
	obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
	var myhtml = obj.contentWindow.document.getElementsByTagName('html')[0];
	myhtml.style.backgroundColor = 'white';
}