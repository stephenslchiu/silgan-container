<?php

/* Don't remove this line. */
define('WP_USE_THEMES', false);
$absolute_path = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
$wp_load = $absolute_path[0] . 'wp-load.php';
require_once($wp_load);

header('Content-type: text/css');
header('Cache-control: must-revalidate');

if (isset($_GET['pid']) && $_GET['pid']):
	$page_bg = get_field('background_image', $_GET['pid']);
?>

@media screen and (min-width: 768px) {
	.body {
		<?php if ($page_bg && !get_field('use_as_feature_image', $_GET['pid'])) echo 'background-image: url(' . $page_bg . ');'; ?>
		<?php if (get_field('background_position', $_GET['pid'])) echo 'background-size: auto; background-position: ' . get_field('background_position', $_GET['pid']); ?>
	}
}


<?php endif; ?>