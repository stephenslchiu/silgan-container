<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

	<!--[if lt IE 8]>
		<div class="alert alert-warning">
			<?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
		</div>
	<![endif]-->

	<?php
		do_action('get_header');
		// Use Bootstrap's navbar if enabled in config.php
		if (current_theme_supports('bootstrap-top-navbar')) {
			get_template_part('templates/header-top-navbar');
		} else {
			get_template_part('templates/header');
		}
	?>

	<div <?php innerbody_class(); ?>>
		<div <?php wrap_class(); ?> role="document">
			<div class="content row">
				<?php if (!get_field('use_as_feature_image') && is_page_template('template-landing.php') && get_field('mobile_image')) : ?>
				<figure class="visible-xs">
					<img src="<?php the_field('mobile_image') ?>" alt="">
				</figure>
				<?php endif; ?>
				<main class="main <?php echo roots_main_class(); ?>" role="main">
					<?php include roots_template_path(); ?>
				</main><!-- /.main -->
				<?php if (get_field('use_as_feature_image')) : ?>
					<aside class="use-as-feature-image <?php echo roots_sidebar_class(); ?>">
						<img src="<?php the_field('background_image') ?>" alt="">
					</aside>
				<?php endif; ?>
			</div><!-- /.content -->
		</div><!-- /.wrap -->
	</div>

	<?php get_template_part('templates/footer'); ?>

</body>
</html>
