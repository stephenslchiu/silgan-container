<?php
/**
 * Enqueue scripts and stylesheets
 *
 * Enqueue stylesheets in the following order:
 * 1. /theme/assets/css/main.min.css
 *
 * Enqueue scripts in the following order:
 * 1. jquery-1.11.0.min.js via Google CDN
 * 2. /theme/assets/js/vendor/modernizr-2.7.0.min.js
 * 3. /theme/assets/js/main.min.js (in footer)
 */
function roots_scripts() {
	wp_enqueue_style('roots_main', get_template_directory_uri() . '/assets/css/main.css', false, '9880649384aea9f1ee166331c0a30daa');
	wp_enqueue_style('flexslider', '//cdn.jsdelivr.net/flexslider/2.2/flexslider.css', false, null);
	wp_enqueue_style('colorbox', '//cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.4.33/example1/colorbox.min.css', false, null);
	wp_enqueue_style('qtip', get_template_directory_uri() . '/assets/css/jquery.qtip.min.css', false, null);
	// wp_enqueue_style('app', get_template_directory_uri() . '/assets/css/app.css', false, null);
	wp_enqueue_style('app', get_template_directory_uri() . '/assets/less/app.less', false, null);
	wp_enqueue_style('custom', get_template_directory_uri() . '/assets/css/custom-style.php?pid=' . get_the_ID(), false, null);
	wp_enqueue_style('fontface', get_template_directory_uri() . '/assets/css/fontface.css', false, null);

	// jQuery is loaded using the same method from HTML5 Boilerplate:
	// Grab Google CDN's latest jQuery with a protocol relative URL; fallback to local if offline
	// It's kept in the header instead of footer to avoid conflicts with plugins.
	if (!is_admin() && current_theme_supports('jquery-cdn')) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', array(), null, false);
		add_filter('script_loader_src', 'roots_jquery_local_fallback', 10, 2);
	}

	if (is_single() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}

	wp_register_script('modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-2.7.0.min.js', array(), null, false);
	wp_register_script('underscorejs', '//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.6.0/underscore-min.js', array(), null, true);
	wp_register_script('sharejs', get_template_directory_uri() . '/assets/js/plugins/jquery.share.js', array(), null, true);
	wp_register_script('flexslider', get_template_directory_uri() . '/assets/js/plugins/jquery.flexslider-min.js', array(), null, true);
	wp_register_script('colorbox', '//cdn.jsdelivr.net/colorbox/1.5.6/jquery.colorbox-min.js', array(), null, true);
	wp_register_script('qtip', get_template_directory_uri() . '/assets/js/plugins/jquery.qtip.min.js', null, true);
	wp_register_script('roots_scripts', get_template_directory_uri() . '/assets/js/scripts.min.js', array(), '0fc6af96786d8f267c8686338a34cd38', true);
	wp_register_script('main', get_template_directory_uri() . '/assets/js/main.js', array(), null, true);
	wp_enqueue_script('modernizr');
	wp_enqueue_script('jquery');
	wp_enqueue_script('underscorejs');
	wp_enqueue_script('sharejs');
	wp_enqueue_script('flexslider');
	wp_enqueue_script('qtip');
	wp_enqueue_script('roots_scripts');
	wp_enqueue_script('main');
}
add_action('wp_enqueue_scripts', 'roots_scripts', 100);

// http://wordpress.stackexchange.com/a/12450
function roots_jquery_local_fallback($src, $handle = null) {
	static $add_jquery_fallback = false;

	if ($add_jquery_fallback) {
		echo '<script>window.jQuery || document.write(\'<script src="' . get_template_directory_uri() . '/assets/js/vendor/jquery-1.11.0.min.js"><\/script>\')</script>' . "\n";
		$add_jquery_fallback = false;
	}

	if ($handle === 'jquery') {
		$add_jquery_fallback = true;
	}

	return $src;
}
add_action('wp_head', 'roots_jquery_local_fallback');

function roots_google_analytics() { ?>
<script>
	(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
	function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
	e=o.createElement(i);r=o.getElementsByTagName(i)[0];
	e.src='//www.google-analytics.com/analytics.js';
	r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
	ga('create','<?php echo GOOGLE_ANALYTICS_ID; ?>');ga('send','pageview');
</script>

<?php }
if (GOOGLE_ANALYTICS_ID && !current_user_can('manage_options')) {
	add_action('wp_footer', 'roots_google_analytics', 20);
}
