<?php

require_once locate_template('/includes/css-crush/CssCrush.php');

add_filter('show_admin_bar', '__return_false');



function wrap_class ($myclass = '') {
	$classes = ' class="wrap ';

	if (is_page_template('template-product-can.php') ||
		is_page_template('template-product-end.php') ||
		is_page_template('template-careers.php') ||
		is_page_template('template-supplier-responsibilities.php') ||
		is_page_template('template-client-downloads.php') ||
		is_page_template('template-vendors-download.php') ||
		is_page_template('template-customer-services.php') ||
		is_page_template('template-silgan-sustainability.php') ||
		is_page_template('template-cans-cooking.php') ||
		is_page_template('template-product-development.php') ||
		is_page_template('template-about-location.php') ||
		is_page_template('template-health-wellness.php')) {
		$classes .= 'container-fluid ';
	} else $classes .= ' container ';

	echo $classes . ' ' . $myclass . '" ';
}




function innerbody_class() {
	$classes = 'class="body';
	if ((!is_page_template() || is_page_template('template-posts.php') || is_page_template('template-sitemap.php')) && is_page()) $classes .= ' default-page ';

	echo $classes . '"';
}








function process_li ($content = '') {
	$content = str_replace('<li>', '<li><span>', $content);
	$content = str_replace('</li>', '</span></li>', $content);
	return $content;
}

function prefix_css ($css = '.foo{}') {
	$css = csscrush_string('.foo { transform:' . $css . ' }');
	$css = str_replace('.foo{', '', $css);
	$css = str_replace('}', '', $css);
	return $css;
}











add_action( 'init', 'register_cpt_image_popup_tip' );

function register_cpt_image_popup_tip() {

	$labels = array( 
		'name' => _x( 'Image popup tips', 'image_popup_tip' ),
		'singular_name' => _x( 'Image popup tip', 'image_popup_tip' ),
		'add_new' => _x( 'Add New', 'image_popup_tip' ),
		'add_new_item' => _x( 'Add New Image popup tip', 'image_popup_tip' ),
		'edit_item' => _x( 'Edit Image popup tip', 'image_popup_tip' ),
		'new_item' => _x( 'New Image popup tip', 'image_popup_tip' ),
		'view_item' => _x( 'View Image popup tip', 'image_popup_tip' ),
		'search_items' => _x( 'Search Image popup tips', 'image_popup_tip' ),
		'not_found' => _x( 'No image popup tips found', 'image_popup_tip' ),
		'not_found_in_trash' => _x( 'No image popup tips found in Trash', 'image_popup_tip' ),
		'parent_item_colon' => _x( 'Parent Image popup tip:', 'image_popup_tip' ),
		'menu_name' => _x( 'Image popup tips', 'image_popup_tip' ),
	);

	$args = array( 
		'labels' => $labels,
		'hierarchical' => false,
		
		'supports' => array( 'title' ),
		
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		
		
		'show_in_nav_menus' => true,
		'publicly_queryable' => false,
		'exclude_from_search' => true,
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post'
	);

	register_post_type( 'image_popup_tip', $args );
}









// [mybutton]Text[/mybutton]
function button_func ($atts, $content = null) {
	extract( shortcode_atts( array(
		'link' => '#',
	), $atts ) );
	return '<a href="' . $link . '" class="btn btn-right-corner">' . $content . '</a>';
}
add_shortcode('mybutton', 'button_func');


function toptab_func ($atts, $content = null) {
	extract( shortcode_atts( array(
		'align' => false,
	), $atts ) );

	if ($align == 'left') return '<h3 class="toptab alignleft">' . $content . '</h3><div class="clear"></div>';
	else return '<h3 class="toptab">' . $content . '</h3>';
}
add_shortcode('toptab', 'toptab_func');


function build_a_can_func () {
	return '<iframe src="' . get_stylesheet_directory_uri() . '/tools/build-a-can/" height="500" width="100%" frameborder="0" scrolling="no"></iframe>';
}
add_shortcode('build_a_can', 'build_a_can_func');


function sus_calculator_func () {
	ob_start();
?>
<section class="one-section sus-section-6">
	<div class="section-wrap">
		<h1 class="section-line-head"><span>SUSTAINABILITY CALCULATOR</span></h1>

		<iframe width="500" height="700" src="<?php echo get_stylesheet_directory_uri(); ?>/tools/sustainability-calculator/" frameborder="0"></iframe>
	</div>
</section>
<?php
	return ob_get_clean();
}
add_shortcode('sus_calculator', 'sus_calculator_func');



// [mytimeline]
function timeline_func ($atts) {
	extract( shortcode_atts( array(
		'with_header' => false,
	), $atts ) );

	if (!$with_header) {
		return '<iframe src="' . get_stylesheet_directory_uri() . '/tools/timeline/?n=1" width="100%" frameborder="0" scrolling="no"></iframe>';
	} else {
		return '<iframe src="' . get_stylesheet_directory_uri() . '/tools/timeline/" width="100%" frameborder="0" scrolling="no"></iframe>';
	}
}
add_shortcode('mytimeline', 'timeline_func');





// [ribbon][/ribbon]
function ribbon_func ($atts, $content = null) {
	return '<div class="section-ribbon">' . $content . '</div>';
}
add_shortcode('ribbon', 'ribbon_func');


















function mytooltip ($image_obj = null, $args = array()) {
	$image_class = 'section-image';
	if ($args) {
		if ($args['class']) $image_class .= ' ' . $args['class'];
	}

	if ($image_obj):
		$popup_tips = get_posts(array(
			'meta_key' => 'image_attached',
			'meta_value' => $image_obj['id'],
			'posts_per_page' => 1,
			'post_type' => 'image_popup_tip',
		));

		if (count($popup_tips) == 0) echo '<img src="' . $image_obj['url'] . '" alt="" class="' . $image_class . '">';
		else {
			$pt = array_shift($popup_tips);
			$tooltips = get_field('tips', $pt->ID);
	?>

	<div class="tooltip-wrapper">
		<span class="tooltip-container">
			<img src="<?php echo $image_obj['url']; ?>" alt="" width="<?php echo $image_obj['width']; ?>" class="<?php echo $image_class; ?>">

			<?php foreach ($tooltips as $mytp) {
				echo '<a ';
				echo 'title="' . $mytp['title'] . '" ';
				foreach ($mytp['attributes'] as $attr) {
					echo ' ' . $attr['name'] . '="' . $attr['value'] . '" ';
				}
				if ($mytp['css_transform_tip']) {
					echo 'data-css-transform="' . prefix_css($mytp['css_transform_tip']) . '" ';
				}
				echo 'style="';
				if ($mytp['position_x']) echo 'left:' . $mytp['position_x'] . '%;';
				if ($mytp['position_y']) echo 'top:' . $mytp['position_y'] . '%;';
				echo '" ';
				echo 'href="#' . sanitize_title($mytp['title']) . '">' . $mytp['title'] . '</a>';
			} ?>
		</span>

		<div class="tooltip-hidden">
		<?php foreach ($tooltips as $mytp) { ?>
			<div id="<?php echo sanitize_title($mytp['title']); ?>">
				<?php echo process_li($mytp['content']); ?>
			</div>
		<?php } ?>
		</div>
	</div>
	<?php
		}
	endif;
}